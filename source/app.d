@safe
import gtk.Application : GtkApplication = Application;
import gtk.ApplicationWindow : Scoped, ApplicationWindow, GApplicationFlags;
import gtk.Button: Button;
import gtk.Box: Box;
import gtk.Dialog:Dialog;
import gtk.DrawingArea : DrawingArea;
import gtk.Label : Label;
import gtk.MainWindow:MessageType,DialogFlags,ResponseType;
import gtk.Widget : Widget;
import gtk.Window:Window;
import gio.Application : GioApplication = Application;
import gdk.c.types : GdkEventMask;
import gdk.Event : Event;
import cairo.Context : Context;

import std.array : split, replace, split;
import std.algorithm.mutation : remove;
import std.conv : to;
import std.format : format;
import std.math : log;
import std.process : executeShell;
import std.stdio : writeln;
import std.string : replace, splitLines, strip;
import std.typecons : Tuple;
import std.process : executeShell;




struct click
{
ulong xmin;
ulong ymin;
ulong xmax;
ulong ymax;
string id;
string disk;
}

click[] clicks;

struct subpartition
{
	string id;
	string atype;
	ulong begin;
	ulong end;
	ulong gap = 0;
}

struct partition
{
	string id;
	string atype;
	ulong begin;
	ulong end;
	ulong gap = 0;
	subpartition[] subpartitions;
}

struct disk
{
	string id;
	string atype;
	ulong begin;
	ulong end;
	partition[] partitions;
}

disk[] disks;
ulong[string] sectorsizes;

void getsectorinfo()
{
	string todo = "gpart list | egrep 'Name|Sectorsize' |sed 'N;s/\\n/ /'  | awk '{print $3,$5}'";
	auto t1 = executeShell(todo);
	string t2 = t1.output;
	string[] t3 = t2.splitLines;
	foreach (num, aline; t3)
	{
		string[] t4 = aline.split;
		sectorsizes[t4[0]] = to!ulong(t4[1]);
	}
}

void getdiskinfo()
{
	string todo = "gpart show -p | egrep 'MBR|GPT' | awk '{print $4,$5,$2,$3}' | grep -v 'p'";
	auto t1 = executeShell(todo);
	string t2 = t1.output;
	string[] t3 = t2.splitLines;
	foreach (num, aline; t3)
	{
		disk adisk;
		auto s = aline.split;
		string id = s[0];
		adisk.id = id;
		ulong sectorsize = sectorsizes[adisk.id];
		string atype = s[1];
		adisk.atype = atype;
		adisk.begin = to!ulong(to!double(s[2]) * sectorsize);
		adisk.end = adisk.begin + to!ulong(to!double(s[3]) * sectorsize);
		disks ~= adisk;
	}
}

void getpartitioninfo()
{
	foreach (num; 0 .. disks.length)
	{
		string todo = "gpart show -p | awk '{print $3,$4,$1,$2}'" ~ " | grep ^" ~ disks[num].id;
		auto t1 = executeShell(todo);
		string t2 = t1.output;
		string[] t3 = t2.splitLines;
		foreach (aline; t3)
		{
			string[] s = aline.split();
			partition p;
			p.id = s[0];
			ulong sectorsize = sectorsizes[p.id];
			p.atype = s[1];
			p.begin = to!ulong(to!double(s[2]) * sectorsize);
			p.end = p.begin + to!ulong(to!double(s[3]) * sectorsize);
			char c = p.id[p.id.length - 1];
			if ((c >= 'a') && (c <= 'z'))
			{
				string shortname = p.id[0 .. p.id.length - 1];
				foreach (ref p2; disks[num].partitions)
				{
					if (p2.id == shortname)
					{
						subpartition s2;
						s2.id = p.id;
						s2.atype = p.atype;
						s2.begin = p.begin;
						s2.end = p.end;
						p2.subpartitions ~= s2;  //modifies p2!!
						ulong mylength = p2.subpartitions.length;
					}
				}
			}
			else
			{
				disks[num].partitions ~= p;
			}
		}
	}
}

void calculategap()
{
	foreach (d, adisk; disks)
	{
		foreach (p, ref apartition; adisk.partitions)
		{
			if (p == 0)
			{
				apartition.gap = apartition.begin; //modifies apartition
			}
			else
			{
				apartition.gap = apartition.begin - adisk.partitions[p - 1].end;
			} //else
			foreach (s, ref asubpartition; apartition.subpartitions)
			{
				if (s == 0)
				{
					asubpartition.gap = asubpartition.begin;  //modifies a subpartition
				}
				else
				{
					asubpartition.gap = asubpartition.begin - apartition.subpartitions[s - 1].end;
				}
			} //s
		} //p
	} //d
} //calculategap

void getfulldiskinfo()
{
	getsectorinfo();
	getdiskinfo();
	getpartitioninfo();
	calculategap();
}

class AreaContent
{
private:
	Box contentArea;
public:	
	this(Box contentArea)
	{
		this.contentArea = contentArea;
		this.contentArea.showAll();

	} // this()
} // class AreaContent


void ispartition(string id,ref long numdisk,ref long numpartition)
{
	foreach(nd,d;disks)
		foreach(np,p;d.partitions)
			{
				if(p.id==id)
					{
						numdisk=nd;
						numpartition=np;
						writeln(id," ",numdisk," ",numpartition);
						return;
					}
			}
return;
}

class MyDialog : Dialog
{
	AreaContent areaContent;
	string id;
private:
	MessageType messageType = MessageType.INFO;
	DialogFlags flags = DialogFlags.MODAL;
	ResponseType[] responseTypes = [ResponseType.YES, ResponseType.NO];
	string[] buttonLabels = ["Yes", "No"];
public:
	this(Window parentWindow,string id,string diskname)
	{
		super("", parentWindow, flags, buttonLabels, responseTypes);
		
		this.setTitle("");
		this.id=id;
		this.setDefaultSize(400,200);
		farmOutContent();
		addbuttons(id,diskname);
		addOnResponse(&doSomething);
		run();
		destroy();
		
	} // this()
private:
	void doSomething(int response, Dialog d)
	{
		switch(response)
		{
			case ResponseType.YES:
				writeln("So, you know where the monkey is."); break;
			
			case ResponseType.NO:
				writeln("You don't know where the monkey is."); break;
			
			
			default: 
				writeln("Dialog closed."); break;
		}
	} // doSomething()
	void farmOutContent()
	{
		areaContent = new AreaContent(this.getContentArea());
		
	}

	
	void addbuttons(string id,string diskname)
	{
		areaContent.contentArea.add(new Label("Manipulate  "~id~"  on disk  "~diskname));
		if(id=="Free")
			{
			areaContent.contentArea.add(new Button("Create partition"));
			}
		else if(id==diskname)
			{
			 areaContent.contentArea.add(new Button("Destroy bootrecord"));
			 areaContent.contentArea.add(new Button("Create MBR table"));
			 areaContent.contentArea.add(new Button("Create GPT table"));
			}
		else
			{
			long numdisk=-1;
			long numpartition=-1;
			ispartition(id,numdisk,numpartition);
			if(numdisk !=-1)
				{
			 	areaContent.contentArea.add(new Button("Delete partition"));
			 	areaContent.contentArea.add(new Button("Format partition"));
				}
			}
		areaContent.contentArea.showAll();

	}
		
} // class MyDialog


class DA : DrawingArea
{
Window win;
public:
	this(Window win)
	{
		addEvents(GdkEventMask.BUTTON_PRESS_MASK);
		addOnDraw(&drawCallback);
		addOnButtonPress(&onButtonPress);
		this.win=win;
	}
	
	/// Allow update of clicks
	static int V=0;
	static immutable ulong scale=1200;
	static immutable string free="Free";

protected:


	void addtoclicks(ulong xmin,ulong ymin,ulong xmax,ulong ymax,string id)
	{
		click c;
		c.xmin=xmin;
		c.ymin=ymin;
		c.xmax=xmax;
		c.ymax=ymax;
		c.id=id;
		clicks~=c;
	}
		

	void drawvierkant(ref Scoped!Context c, ulong x, ulong y, long xs, long ys,string id)
	{
		c.setLineWidth(3);
		c.moveTo(x, y);
		c.lineTo(x, y + ys);
		c.lineTo(x + xs, y + ys);
		c.lineTo(x + xs, y);
		c.lineTo(x, y);
		c.stroke();
		if(V==0) addtoclicks(x,y,x+xs,y+ys,id);
	}

	void drawdisks(ref Scoped!Context c, ulong num, disk d)
	{
		c.setSourceRgba(1, 0, 0, 1);
		drawvierkant(c, 100, 100 + num * 75, 50, 50,d.id);
		c.setFontSize(12);
		c.moveTo(100 + 10, 100 + num * 75 + 20);
		c.showText(d.id);
		c.moveTo(100 + 10, 100 + num * 75 + 40);
		c.showText(d.atype);
	}

	double partitionscale(disk d)
	{
		double total = 0;
		foreach (pt, p; d.partitions)
		{
			if (p.gap > 0)
				total += log(p.gap);
			total += log(p.end - p.begin);
		}
		if (d.end - d.partitions[d.partitions.length - 1].end > 0)
			total += log(d.end - d.partitions[d.partitions.length - 1].end);
		return total;
	}

	void drawfreebegin(ref Scoped!Context c, ulong num, partition p, double begin,
			double total)
	{
		double length = log(p.gap);
		ulong ubegin = to!ulong(begin * scale / total);
		ulong ulength = to!ulong(length * scale / total);
		c.setSourceRgba(0, 0, 1, 1);
		drawvierkant(c, 175 + ubegin, 100 + num * 75, ulength - 5, 50,free);
		c.moveTo(175 + ubegin + 5, 100 + num * 75 + 20);
		c.showText(free);
		c.moveTo(175 + ubegin + 5, 100 + num * 75 + 40);
		string text = format!("%,s")(p.gap / 1000);
		c.showText(text);
	}

	void drawpartition(ref Scoped!Context c, ulong num, partition p, double begin,
			double total)
	{
		double length = log(p.end - p.begin);
		ulong ubegin = to!ulong(begin * scale / total);
		ulong ulength = to!ulong(length * scale / total);
		c.setSourceRgba(0, 0, 0, 1);
		drawvierkant(c, 175 + ubegin, 100 + num * 75, ulength - 5, 50,p.id);
		c.moveTo(175 + ubegin + 10, 100 + num * 75 + 13);
		c.showText(p.id);
		c.moveTo(175 + ubegin + 10, 100 + num * 75 + 28);
		c.showText(p.atype);
		c.moveTo(175 + ubegin + 10, 100 + num * 75 + 43);
		string s=format!("%,s")(p.end-p.begin);
		c.showText(s);
	}

	void drawsubpartitions(ref Scoped!Context c, ulong num, partition p,
			double begin, double total, double length, double subtotal)
	{
		ulong ubegin2 = to!ulong(begin * scale / total);
		ulong ubegin = to!ulong(begin * scale / total);
		ulong ulength = to!ulong(length * scale / total);
		foreach (p2; p.subpartitions)
		{
			if (p2.gap > 0)
			{
				ulong L = to!ulong(ulength * log(p2.gap) / subtotal);
				c.setSourceRgba(1, 0, 0, 1);
				drawvierkant(c, 177 + ubegin2, 105 + num * 75, L - 5, 40,free);
				ubegin2 = ubegin2 + L;
			} //if
			ulong L = to!ulong(ulength * log(p2.end - p2.begin) / subtotal);
			c.setSourceRgba(0, 1, 0, 1);
			drawvierkant(c, 177 + ubegin2, 105 + num * 75, L - 5, 40,p2.id);
			ubegin2 = ubegin2 + L;
		} //for
		if ((p.subpartitions.length >= 1)
				&& (p.end - p.subpartitions[p.subpartitions.length - 1].end > 0))
		{
			ulong L = to!ulong(ulength * log(
					p.end - p.subpartitions[p.subpartitions.length - 1].end) / subtotal);
			c.setSourceRgba(1, 0, 0, 1);
			drawvierkant(c, 177 + ubegin2, 105 + num * 75, L - 5, 40,free);
			ubegin2 = ubegin2 + L;
		}
	} //drawsubpartitions

	void drawfreeend(ref Scoped!Context c, ulong num, disk d, double total,
			double begin)
	{
		ulong ubegin = to!ulong(begin * scale / total);
		double length = log(d.end - d.partitions[d.partitions.length - 1].end);
		ulong ulength = to!ulong(length * scale / total);
		c.setSourceRgba(0, 0, 1, 1);
		drawvierkant(c, 175 + ubegin, 100 + num * 75, ulength - 5, 50,free);
		c.moveTo(175 + ubegin + 5, 100 + num * 75 + 20);
		c.showText(free);
		c.moveTo(175 + ubegin + 5, 100 + num * 75 + 40);
		string text = format!("%,s")((d.end - d.partitions[d.partitions.length - 1].end) / 1000);
		c.showText(text);
	}

	bool drawCallback(Scoped!Context c, Widget w)
	{
		foreach (num, d; disks)
		{
			drawdisks(c, num, d);
			double total = partitionscale(d);
			double begin = 0;
			foreach (pt, p; d.partitions)
			{
				if (p.gap > 0)
				{
					drawfreebegin(c, num, p, begin, total);
					begin += log(p.gap);
				} //if
				drawpartition(c, num, p, begin, total);
				//----------------------------------------------------
				double subtotal = 0;
				foreach (p2; p.subpartitions)
				{
					if (p2.gap > 0)
						subtotal += log(p2.gap);
					subtotal += log(p2.end - p2.begin);
				}
				if ((p.subpartitions.length >= 1)
						&& (p.end - p.subpartitions[p.subpartitions.length - 1].end > 0))
					subtotal += log(p.end - p.subpartitions[p.subpartitions.length - 1].end);
				double length = log(p.end - p.begin);
				drawsubpartitions(c, num, p, begin, total, length, subtotal);
				begin += length;
			} //foreach partition
			//------------------------------------------------------
			if (d.end - d.partitions[d.partitions.length - 1].end > 0)
				drawfreeend(c, num, d, total, begin);
		} //foreach disk
		V=1;//Do not update clicks
		return true;
	}


	void findclick(ref bool found,ref string id,double x,double y)
	{
		writeln("x: ",x," y:",y);
		foreach(c; clicks)
			{
				if( (c.xmin < x) &&
				    (c.xmax > x) &&
				    (c.ymin < y) &&
				    (c.ymax > y))
				{
					found=true;
					id=c.id;
					return;			
				}
			}
		found=false;
		id="";
	}

	public bool onButtonPress(Event event, Widget widget)
	{
		double x, y;
		event.getCoords(x, y);
		bool found;
		string id;
		findclick(found,id,x,y);
		if(found==true)
		{
			writeln(id);
			ulong numdisk=to!ulong((y-100)/75);
			string diskname=disks[numdisk].id;
			MyDialog mydialog = new MyDialog(win,id,diskname);
		}
				
		return false;
	}

}

int main(string[] args)
{

	getfulldiskinfo();
	GtkApplication gtkapp;
	string title = "org.MyApp";

	void activateGui(GioApplication gioapp)
	{
		ApplicationWindow win = new ApplicationWindow(gtkapp);
		win.setTitle(title);
		win.setDefaultSize(1400, 500);
		auto da = new DA(win);
		win.add(da);
		da.show();
		win.showAll();
	}

	gtkapp = new GtkApplication(title, GApplicationFlags.FLAGS_NONE);
	gtkapp.addOnActivate(&activateGui);
	return gtkapp.run(args);
}

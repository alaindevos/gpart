import cairo.Context:Context;
import gtk.Widget:Widget;
import gtk.DrawingArea:DrawingArea;
import gtk.Application: GtkApplication=Application;
import gtk.ApplicationWindow:Scoped,ApplicationWindow,GApplicationFlags;
import gio.Application: GioApplication=Application;


import std.stdio:writeln;
import std.process:executeShell;
import std.typecons:Tuple;
import std.array:split;
import std.algorithm.mutation:remove;
import std.string:replace;

class DA:DrawingArea {
public:
	this()
		{ addOnDraw(&drawCallback);}

protected:
	bool drawCallback(Scoped!Context c,Widget w){
		c.setSourceRgba(0.5,0.5,0.5,0.5);
		c.setLineWidth(3);
		c.arc(125,125,25,0,6.28);
		c.stroke();
		return true;
	}
}

int main(string [] args)
{
	auto s1=executeShell("sysctl kern.disks");
	int i=s1.status;
	string s2=s1.output;
	string[] s3=split(s2);
	remove(s3,0);
	foreach(string x ; s3)
		{
			auto t1=executeShell("gpart show -p "~x);
			string t2=t1.output;
			t2=t2.replace("K)","_000)");
			t2=t2.replace("M)","_000_000)");
			t2=t2.replace("G)","_000_000_000)");
			t2=t2.replace("T)","_000_000_000_000)");
			t2=t2.replace(".0_","_");
			t2=t2.replace(".1_","_");
			t2=t2.replace(".2_","_");
			t2=t2.replace(".3_","_");
			t2=t2.replace(".4_","_");
			t2=t2.replace(".5_","_");
			t2=t2.replace(".6_","_");
			t2=t2.replace(".7_","_");
			t2=t2.replace(".8_","_");
			t2=t2.replace(".9_","_");
			writeln(t2);
		}
		
	GtkApplication application;
	
	void activateClock(GioApplication app)
	{
		ApplicationWindow win = new ApplicationWindow(application);

		win.setTitle("gtkD Cairo Clock");
		win.setDefaultSize( 250, 250 );

		auto da = new DA();
		win.add(da);
		da.show();
		win.showAll();
	}
	
	application = new GtkApplication("org.gtkd.demo.cairo.clock", GApplicationFlags.FLAGS_NONE);
	application.addOnActivate(&activateClock);
	return application.run(args);
	
}
